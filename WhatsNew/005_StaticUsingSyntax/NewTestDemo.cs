﻿using System;

namespace WhatsNew._005_StaticUsingSyntax
{
    using NUnit.Framework;
    using static DemoClass;

    [TestFixture]
    public class NewTestDemo
    {
        [Test]
        public void T001_NewWayOfCalling()
        {
            Assert.AreEqual("Ala ma kota", ReturnSomeText());
        }
    }
}