﻿namespace WhatsNew._004_ExpressionBodiedFunctionsAndProperties
{
    public class ExpressionBodyOld
    {
        public ExpressionBodyOld(string input)
        {
            SomePropertyValue = input;
        }

        public string SomePropertyValue { get; private set; }

        public string SomeSimpleMethod()
        {
            return string.Format("{0}x{1}", SomePropertyValue, SomePropertyValue);
        }
    }
}