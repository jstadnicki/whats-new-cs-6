﻿using NUnit.Framework;

namespace WhatsNew._001_NullConditionalOperator
{
    [TestFixture]
    public class NullConditionalOperatorTests
    {
        [Test]
        public void T001_TestWithIf_DoesNotThrowOnNull()
        {
            string s = null;
            Assert.DoesNotThrow(() => NullConditionalOperator.TruncateWithIf(s));
        }

        [Test]
        public void T002_TestWith6_DoesNotThrowOnNull()
        {
            string s = null;
            Assert.DoesNotThrow(() => NullConditionalOperator.TruncateWithIf(s));
        }

        [Test]
        public void T003_TestWithIf_Trims()
        {
            string s = "        asd  ";
            var result = NullConditionalOperator.TruncateWithIf(s);
            Assert.AreEqual("asd", result);
        }

        [Test]
        public void T004_TestWithIf_Trims()
        {
            string s = "        asd  ";
            var result = NullConditionalOperator.TruncateWith6(s);
            Assert.AreEqual("asd", result);
        }


        [Test]
        public void T005()
        {
            Assert.DoesNotThrow(() => NullConditionalOperator.TrimChildTextWithIf(null));
        }

        [Test]
        public void T006()
        {
            Assert.DoesNotThrow(() => NullConditionalOperator.TrimChildTextWith6(null));
        }

        [Test]
        public void T007()
        {
            var sut = new Node();
            Assert.DoesNotThrow(() => NullConditionalOperator.TrimChildTextWithIf(sut));
        }

        [Test]
        public void T008()
        {
            var sut = new Node();
            Assert.DoesNotThrow(() => NullConditionalOperator.TrimChildTextWith6(sut));
        }

        [Test]
        public void T009()
        {
            var sut = new Node();
            sut.Child = new Node();
            Assert.DoesNotThrow(() => NullConditionalOperator.TrimChildTextWithIf(sut));
        }

        [Test]
        public void T010()
        {
            var sut = new Node();
            sut.Child = new Node();
            Assert.DoesNotThrow(() => NullConditionalOperator.TrimChildTextWith6(sut));
        }

        [Test]
        public void T011()
        {
            var sut = new Node();
            sut.Child = new Node();
            sut.Child.Text = " aslkdjlaskjd                ";
            var result = NullConditionalOperator.TrimChildTextWithIf(sut);
            Assert.AreEqual("aslkdjlaskjd", result);
        }

        [Test]
        public void T012()
        {
            var sut = new Node();
            sut.Child = new Node();
            sut.Child.Text = " aslkdjlaskjd                ";
            var result = NullConditionalOperator.TrimChildTextWith6(sut);
            Assert.AreEqual("aslkdjlaskjd", result);
        }

        [Test]
        public void T013_Null_Returns_Empty_String()
        {
            var result = NullConditionalOperator.TrimChildTextWithIf(null);
            Assert.NotNull(result);
        }

        [Test]
        public void T014_Null_Returns_Empty_String()
        {
            var result = NullConditionalOperator.TrimChildTextWith6(null);
            Assert.NotNull(result);
        }
    }
}
