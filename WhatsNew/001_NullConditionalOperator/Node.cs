namespace WhatsNew._001_NullConditionalOperator
{
    public class Node
    {
        public Node Child { get; set; }
        public string Text { get; set; }
    }
}