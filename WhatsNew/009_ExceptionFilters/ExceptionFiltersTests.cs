﻿namespace WhatsNew._009_ExceptionFilters
{
    using System;
    using NUnit.Framework;

    [TestFixture]
    public class ExceptionFiltersTests
    {
        [Test]
        public void T001_Handle_Old()
        {
            var sut = ExceptionThrowAndCatch.RunOld("500");
            Assert.AreEqual("handled", sut);
        }

        [Test]
        public void T002_Handle_New()
        {
            var sut = ExceptionThrowAndCatch.RunNew("500");
            Assert.AreEqual("handled", sut);
        }

        [Test]
        public void T003_Throws_Old()
        {
            try
            {
                var sut = ExceptionThrowAndCatch.RunOld("999");
            }
            catch (Exception e)
            {
                Assert.AreEqual("500", e.Message);
            }
        }

        [Test]
        public void T004_Throws_New()
        {
            try
            {
                var sut = ExceptionThrowAndCatch.RunNew("999");
            }
            catch (Exception e)
            {
                Assert.AreEqual("500", e.Message);
            }
        }
    }
}