﻿using System;
using NUnit.Framework;

namespace WhatsNew._008_String_Interpolation
{
    [TestFixture]
    public class StringInterpolationTests
    {
        [Test]
        public void T001_OldWayStringFormat()
        {
            var price = 123d;
            var date = new DateTime(2000, 1, 10, 12, 30, 3);

            var result = TextPrinter.OldFormatText(price, date);

            Assert.AreEqual(result,"2000-01-10 123,00 zł");
        }

        [Test]
        public void T002_NewWayStringFormat()
        {
            var price = 123;
            var date = new DateTime(2000, 1, 10, 12, 30, 3);

            var result = TextPrinter.NewFormatText(price, date);

            Assert.AreEqual(result, "2000-01-10 123,00 zł");
        }
    }
}