﻿namespace WhatsNew._001_NullConditionalOperator
{
    public static class NullConditionalOperator
    {
        public static string TruncateWithIf(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                return string.Empty;
            }
            return input.Trim();
        }

        public static string TruncateWith6(string input)
        {
            return input?.Trim();
        }

        public static string TrimChildTextWithIf(Node item)
        {
            if (item == null)
            {
                return string.Empty;
            }
            if (item.Child == null)
            {
                return string.Empty;
            }
            if (string.IsNullOrEmpty(item.Child.Text))
            {
                return string.Empty;
            }

            return item.Child.Text.Trim();
        }

        public static string TrimChildTextWith6(Node item)
        {
            return item?.Child?.Text?.Trim() ?? string.Empty;
        }
    }
}