﻿namespace WhatsNew._002_NameOfExpressions
{
    public class NameOfExpressionOld
    {
        public static string ParameterName(object o)
        {
            return "inputParameter";
        }

        public static string ClassName()
        {
            return "NameOfExpression";
        }

        [NameOfExpressionDemo]
        public static string AttributeName()
        {
            return "NameOfExpressionDemoAttribute";
        }
    }
}