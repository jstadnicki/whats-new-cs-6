﻿namespace WhatsNew._005_StaticUsingSyntax
{
    using NUnit.Framework;

    [TestFixture]
    public class OldTestDemo
    {
        [Test]
        public void T001_OldWayOfCalling()
        {
            Assert.AreEqual("Ala ma kota", DemoClass.ReturnSomeText());
        }
    }
}