﻿using System;
using System.Threading.Tasks;

namespace WhatsNew._010_AsyncCatch
{
    public static class TestClass
    {
        public static async Task ThrowsAsync()
        {
            throw new Exception("from async");
        }

        public static async Task CatchAsync()
        {
            Task.Run(() => { });
        }

        public static async Task FinallyAsync()
        {
            Task.Run(() => { });
        }
    }
}