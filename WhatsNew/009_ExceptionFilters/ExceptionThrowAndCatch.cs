using System;

namespace WhatsNew._009_ExceptionFilters
{
    public class ExceptionThrowAndCatch
    {
        public static string RunOld(string s)
        {
            try
            {
                throw new Exception("500");
            }
            catch (Exception e)
            {
                if (e.Message == s)
                {
                    return "handled";
                }

                if (string.IsNullOrEmpty(e.Message))
                {
                    return string.Empty;
                }

                throw;
            }
        }

        public static string RunNew(string s)
        {
            try
            {
                throw new Exception("500");

            }
            catch (Exception e) when (e.Message == s)
            {
                return "handled";
            }
            catch (Exception e) when (string.IsNullOrEmpty(e.Message))
            {
                return string.Empty;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}