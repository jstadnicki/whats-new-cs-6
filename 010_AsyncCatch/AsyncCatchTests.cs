﻿using System;
using NUnit.Framework;
using _010_AsyncCatch;

namespace WhatsNew._010_AsyncCatch
{
    [TestFixture]
    public class AsyncCatchTests
    {
        [Test]
        public async void T001_Compiles()
        {
            try
            {
                await TestClass.ThrowsAsync();
            }
            catch (Exception)
            {

                await TestClass.CatchAsync();
            }
            finally
            {
                await TestClass.FinallyAsync();
            }
        }

    }
}