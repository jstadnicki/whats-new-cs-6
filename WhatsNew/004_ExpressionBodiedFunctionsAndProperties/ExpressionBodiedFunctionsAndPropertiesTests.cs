﻿using NUnit.Framework;

namespace WhatsNew._004_ExpressionBodiedFunctionsAndProperties
{
    [TestFixture]
    public class ExpressionBodiedFunctionsAndPropertiesTests
    {
        [Test]
        public void T001_GetPropertyValueOld()
        {
            var sut = new ExpressionBodyOld("old dog");
            Assert.AreEqual("old dog", sut.SomePropertyValue);
        }

        [Test]
        public void T002_GetPropertyValueNew()
        {
            var sut = new ExpressionBodyNew("new trick");
            Assert.AreEqual("new trick", sut.SomePropertyValue);
        }

        [Test]
        public void T003_GetMethodOld()
        {
            var sut = new ExpressionBodyOld("A");
            Assert.AreEqual("AxA", sut.SomeSimpleMethod());
        }

        [Test]
        public void T004_GetMethodNew()
        {
            var sut = new ExpressionBodyNew("V");
            Assert.AreEqual("VxV", sut.SomeSimpleMethod());
        }
    }
}