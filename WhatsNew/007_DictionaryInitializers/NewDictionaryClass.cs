﻿using System.Collections.Generic;

namespace WhatsNew._007_DictionaryInitializers
{
    public class NewDictionaryClass
    {

        public NewDictionaryClass()
        {
            dictionary = new Dictionary<string, string>
            {
                ["Hello"] = "World",
                ["Si"] = "Sharp"
            };
        }

        private Dictionary<string, string> dictionary;
        public string this[string key]
        {
            get { return this.dictionary[key]; }
        }
    }
}