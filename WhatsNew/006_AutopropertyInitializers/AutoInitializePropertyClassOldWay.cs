namespace WhatsNew._006_AutopropertyInitializers
{
    public class AutoInitializePropertyClassOldWay
    {
        public AutoInitializePropertyClassOldWay()
        {
            PropertyValue = "hello si sharp";
        }

        public string PropertyValue { get; set; }
    }
}