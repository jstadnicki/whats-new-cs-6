﻿namespace WhatsNew._002_NameOfExpressions
{
    public class NameOfExpression
    {
        public static string ParameterName(string inputParameter)
        {
            return nameof(inputParameter);
        }

        [NameOfExpressionDemo]
        public static string ClassName()
        {
            return nameof(NameOfExpression);
        }

        public static string AttributeName()
        {
            return nameof(NameOfExpressionDemoAttribute);
        }
    }
}