﻿namespace WhatsNew._007_DictionaryInitializers
{
    using NUnit.Framework;

    [TestFixture]
    public class DictionaryInitializersTests
    {
        [Test]
        public void T001_InitializeDictionaryOldWay()
        {
            var sut = new OldDictionaryClass();
            Assert.AreEqual(sut["Hello"], "World");
            Assert.AreEqual(sut["Si"], "Sharp");
        }

        [Test]
        public void T002_InitializeDictionaryOldWay()
        {
            var sut = new NewDictionaryClass();
            Assert.AreEqual(sut["Hello"], "World");
            Assert.AreEqual(sut["Si"], "Sharp");
        }
    }
}