using System;

namespace WhatsNew._008_String_Interpolation
{
    public class TextPrinter
    {
        public static string OldFormatText(double price, DateTime date)
        {
            return string.Format("{0:d} {1:C}", date, price);
        }

        public static string NewFormatText(double price, DateTime date)
        {
            return $"{date:d} {price:C}";
        }
    }
}