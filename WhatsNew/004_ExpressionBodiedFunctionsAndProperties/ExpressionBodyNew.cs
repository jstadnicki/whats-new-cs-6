﻿namespace WhatsNew._004_ExpressionBodiedFunctionsAndProperties
{
    public class ExpressionBodyNew
    {
        private readonly string _somePropertyValue;

        public string SomePropertyValue => _somePropertyValue;

        public ExpressionBodyNew(string input)
        {
            _somePropertyValue = input;
        }

        public string SomeSimpleMethod() => string.Format("{0}x{0}", SomePropertyValue, SomePropertyValue);
    }
}