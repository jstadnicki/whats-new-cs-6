﻿using NUnit.Framework;

namespace WhatsNew._006_AutopropertyInitializers
{
    [TestFixture]
    public class AutoInitializePropertyTests
    {
        [Test]
        public void T001_InitializeProperty()
        {
            var sut = new AutoInitializePropertyClassOldWay();
            Assert.AreEqual("hello si sharp", sut.PropertyValue);
        }

        [Test]
        public void T002_InitializeProperty()
        {
            var sut = new AutoInitializePropertyClassNewWay();
            Assert.AreEqual("hello si sharp", sut.PropertyValue);
        }
    }
}