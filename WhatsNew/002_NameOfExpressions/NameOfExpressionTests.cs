﻿using NUnit.Framework;

namespace WhatsNew._002_NameOfExpressions
{
    [TestFixture]
    public class NameOfExpressionTests
    {
        [Test]
        public void T001_ParameterName()
        {
            Assert.AreEqual("inputParameter", NameOfExpression.ParameterName(null));
        }

        [Test]
        public void T002_ClassName()
        {
            Assert.AreEqual("NameOfExpression", NameOfExpression.ClassName());
        }

        [Test]
        public void T003_AttributeName()
        {
            Assert.AreEqual("NameOfExpressionDemoAttribute", NameOfExpression.AttributeName());
        }

        [Test]
        public void T004_ParameterName()
        {
            Assert.AreEqual("inputParameter", NameOfExpressionOld.ParameterName(null));
        }

        [Test]
        public void T005_ClassName()
        {
            Assert.AreEqual("NameOfExpression", NameOfExpressionOld.ClassName());
        }

        [Test]
        public void T006_AttributeName()
        {
            Assert.AreEqual("NameOfExpressionDemoAttribute", NameOfExpressionOld.AttributeName());
        }
    }
}